// import required essentials
const express = require('express');
// create new router
const router = express.Router();

// create a JSON data array
let data = [
    // { id: 1, title: 'Create a project',  order: 1, completed: true }
    {  
        "id": 1,
        "IsValidToken": true,
        "Success": true,
        "StatusCode": 200,
        "Message": "success",
        "Data": [
            {
                "game_name": "CarromRMG",    
                "config": 
                { 
                    "flurry": {
                        "enable": true,
                        "key": "RF92JWRNQMHCBMNY3T6P"
                    }, 
                    "firebase": {
                        "enable": true
                    },  
                    "rate_us": {
                        "enable": false,
                        "debug": false,
                        "device_ids": "",
                        "excluded_versions": "",
                        "title": "Rate This Game!",
                        "message": "If you enjoy playing this game, would you mind taking a moment to rate it? It won't take more than a minute. Thanks you for your support.",
                        "icon_name": "",
                        "bg_img": "",
                        "first_launch_session": 1,
                        "later_session": 2,
                        "no_thanks_session": -1
                    },
                    "force_update": {
                        "enable": false,
                        "debug": true,
                        "device_ids": "",
                        "play_store_version": "7.4",
                        "excluded_versions": "7.4",
                        "skip_excluded_versions": "1.0;2.0;3.0;4.0;5.0;5.1;5.2;5.3;5.4;5.5;5.6;5.7;5.8;5.9;6.0;6.1;1.1;1.2;1.3;1.4;6.2;6.3;6.4;6.5;6.6;6.7;6.8;6.9;7.0;7.1;7.2;7.3",
                        "title": "New Update Available!   v7.4",
                        "message": "- This update includes performance improvements and bug fixes to make game better for you.\n- Now Practice more to improve your skills.\n- More Exciting Prizes in Loyalty.\n- Deposit more, Win more.\n\n- If you face any issue in updating the app, visit https://carromclash.com/ & install the latest app.",
                        "filelink": "http://13.234.206.92/carrom-rivals/apk/Carrom_Clash_7_4.apk?&referrer=&utm_source=ForceUpdate&utm_medium=&utm_term=&utm_content=&utm_campaign=",
                        "filename": "CarromClash7_4.apk"
                    },
                    "inapp_message": {
                        "enable": false,
                        "debug": true,
                        "device_ids": "",
                        "excluded_versions": "2.0;3.0;4.0;5.0;5.1;5.2;5.3;5.4;5.5;5.6;5.7;5.8;5.9;6.0;1.3;1.2;1.1;6.1;6.2;6.3;6.4;6.5;6.6;6.7;6.8;6.9;7.0;7.1;7.2",
                        "title": "Important Announcement!",
                        "message": "We have removed AIM Buy option from shop, so please use your owned AIM today only because from tomorrow we will remove AIM Feature and everyone will be able to use AIM by default in game.",
                        "image_url": "",
                        "session_count": "2",
                        "message_version": "9.6"
                    },
                    "firebase_referral": {
                        "enable_new": true,
                        "excluded_versions": "",
                        "link_domain": "",
                        "package_name": "",
                        "title": "Refer Game",
                        "message": "Earn Upto Rs.1000 on sharing with friends!;You get Rs.10 whenever a friend register and play first game using your code/link. You also get 50% of their first deposit whenever they deposit. Please wait for 10 minutes for balance to refresh.\nNote: You can claim up to Rs.1000 with Refer & Earn",
                        "play_store_url": "https://play.google.com/store/apps/details?id=com.theappguruz.nazara.carrom",
                        "utm_source": "CarromClahsRivals",
                        "referral_points": "5",
                        "show_log": true,
                        "share_via": "whatsapp",
                        "referral_base_url": "http://13.126.241.0:5000"
                    },
                    "device_root_check": {
                        "enable": true,
                        "rootBeerCheck": true,
                        "parallelEmulatorCheck": true,
                        "rootDeviceText": "Your device is either rooted or has apps like GameGuardian / ParallelSpace which are not allowed. Please unroot your phone or uninstall Gameguardian or play with another device!",
                        "tp_packages_name": "com.parallel.space.pro,com.parallel.space.lite,com.lbe.parallel.intl,com.parallel.space.pro.arm64,com.lbe.parallel.intl.arm64"
                    },
                    "daily_spin": {
                        "enable": true,
                        "ds_text": ""
                    },
                    "firebase_performance": {
                        "enable": true
                    },
                    "singular": {
                        "enable": true,
                        "apiKey": "nazaratechnologiesprivatelimited_d72449df",
                        "secretKey": "88cb15b2557f02c585c3ea7a4df4976e"
                    },
                    "gameanalytics": {
                        "enable": true,
                        "showLog": true,
                        "gameKey": "30b66ebcdaab2bcaf1e55465d7866c48",
                        "secretKey": "56e0bfbff51941450aec20ae48f72c13bf8c43fb"
                    }
                }
            }
        ]
    }
 ] ;

// this end-point of an API returns JSON data array
router.get('/', function (req, res) {
    res.status(200).json(data);
});

// this end-point returns an object from a data array find by id
// we get `id` from URL end-points
router.get('/:id', function (req, res) {
    // find an object from `data` array match by `id`
    let found = data.find(function (item) {
        return item.id === parseInt(req.params.id);
    });
    // if object found return an object else return 404 not-found
    if (found) {
        res.status(200).json(found);
    } else {
        res.sendStatus(404);
    }
});

router.put('/:id', function (req, res) {
    // get item object match by `id`
    let found = data.find(function (item) {
        return item.id === parseInt(req.params.id);
    });

    // check if item found
    if (found) {
        let updated = {
            id: found.id,
            IsValidToken: req.body.IsValidToken,
            Success: req.body.Success,
            StatusCode: req.body.StatusCode,
            Message: req.body.Message,
            Data:req.body.Data,
            // title: req.body.title, // set value of `title` get from req
            // order: req.body.order, // set value of `order` get from req
            // completed: req.body.completed // set value of `completed` get from req
        };

        // find index of found object from array of data
        let targetIndex = data.indexOf(found);

        // replace object from data list with `updated` object
        data.splice(targetIndex, 1, updated);

        // return with status 204
        // success status response code 204 indicates
        // that the request has succeeded
        res.sendStatus(204);
    } else {
        res.sendStatus(404);
    }
});

module.exports = router;